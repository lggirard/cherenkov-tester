// Include files
#include "RichTbGeometryParameters.hh"
#include "RichTbMiscNames.hh"
#include "RichTbMaterial.hh"
#include "RichTbLHCbR1FlatMirror.hh"
#include "G4RotationMatrix.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4SubtractionSolid.hh"
#include "G4Box.hh"
#include "G4Sphere.hh"
#include "G4PVPlacement.hh"
#include "RichTbRunConfig.hh"
#include "RichTbMaterial.hh"
#include "G4IntersectionSolid.hh"
#include "G4Orb.hh"
#include "G4Cons.hh"
#include "G4Para.hh"
#include "G4Trd.hh"
#include "G4Tubs.hh"
#include "G4Torus.hh"
#include "U.hh"


#include <iostream>
//-----------------------------------------------------------------------------
// Implementation file for class : RichTbLHCbR1FlatMirror
// SE 22-07-2021
//--------------------------------------------------------
RichTbLHCbR1FlatMirror::RichTbLHCbR1FlatMirror(RichTbLHCbRich1SubMaster* rTbLHCbR1SubMaster) {
  mTbLHCbR1SubMaster = rTbLHCbR1SubMaster;
  constructLbR1FlatMirror();
}

RichTbLHCbR1FlatMirror::~RichTbLHCbR1FlatMirror() {; }

void RichTbLHCbR1FlatMirror::constructLbR1FlatMirror() {
  RichTbMaterial* aMaterial = RichTbMaterial::getRichTbMaterialInstance();
  
  int mode = U::getenvint(envkey, 0);
  
G4VSolid* solid = NULL;


switch(mode)
  {
 default: {//current mirror
      const G4double& InnerRadius  = RichTbR1FlatMirrInnerRadius ; 
      const G4double& OuterRadius  = RichTbR1FlatMirrOuterRadius ; 
      const G4double& Thickness    = RichTbR1FlatMirrThickness ; 
      const G4double& SegmentSizeY = RichTbR1FlatMirrSegmentSizeY ; 
      const G4double& SegmentSizeZ = RichTbR1FlatMirrSegmentSizeX ;   // the SizeX is misnamed, should be SizeZ 

      const G4double SagittaMax = 1.*CLHEP::mm ;  // see ~/opticks/extg4/X4SolidMaker.cc X4SolidMaker::LHCbRichFlatMirr
      const G4double FullDepthX =  Thickness + SagittaMax ;  
      const G4double MiddleRadius = (InnerRadius + OuterRadius)/2. ; 

      std::cout 
          << "RichTbLHCbR1FlatMirror::constructLbR1FlatMirror"
          << " mode " << setting 
          << " InnerRadius " << InnerRadius  
          << " OuterRadius " << OuterRadius  
          << " MiddleRadius " << MiddleRadius  
          << " Thickness " << Thickness 
          << " FullDepthX " << FullDepthX 
          << " SegmentSizeY " << SegmentSizeY
          << " SegmentSizeZ " << SegmentSizeZ
          << std::endl 
          ; 

      G4Orb* inner = new G4Orb("inner",InnerRadius); 
      G4Orb* outer = new G4Orb("outer",OuterRadius); 
      G4SubtractionSolid* shell = new G4SubtractionSolid("shell", outer, inner ); 
      G4Box* box = new G4Box("box", FullDepthX/2. , SegmentSizeY/2. , SegmentSizeZ/2. );   
      G4String rootname = "RichTbR1FlatFull" ; 
      rootname += "_CSG_EXBB" ; 
      G4VSolid* alt = new G4IntersectionSolid( rootname, shell, box, 0, G4ThreeVector( MiddleRadius, 0., 0. ));    

      solid = alt ; 
}break;
case 1: {//whole sphere
solid =  new G4Sphere ("whole sphere", 0.0, RichTbR1FlatMirrOuterRadius, 0.0, 360.0, 0.0, 180.0);
}break;
case 2:{ //hollow sphere
solid =  new G4Sphere ("hollow sphere", RichTbR1FlatMirrInnerRadius,
                                           RichTbR1FlatMirrOuterRadius, 0.0, 360.0, 0.0, 180.0);
  }break;
case 3: {//0 to theta
solid =  new G4Sphere ("0 to theta", 0.0,
                                           RichTbR1FlatMirrOuterRadius, 0.0, 360.0, 0.0,
                                           RichTbR1FlatMirrStartTheta);
}break;
case 4:{ //theta to 180
solid =  new G4Sphere ("theta to 180",0.0,
                                           RichTbR1FlatMirrOuterRadius,0.0, 360.0, RichTbR1FlatMirrStartTheta,
                                           180.0 - RichTbR1FlatMirrStartTheta);
}break;
case 5: {//theta
solid =  new G4Sphere ("theta",0.0,
                                           RichTbR1FlatMirrOuterRadius, 0.0, 360.0, RichTbR1FlatMirrStartTheta,
                                           RichTbR1FlatMirrDeltaTheta);
}break;
case 6: {//0 to phi
solid =  new G4Sphere ("0 to phi", 0.0,
                                           RichTbR1FlatMirrOuterRadius, 0.0, RichTbR1FlatMirrStartPhi, 0.0,
                                           180.0);
}break;
case 7: {//phi to 360.0
solid =  new G4Sphere ("phi to 360",0.0,
                                           RichTbR1FlatMirrOuterRadius,RichTbR1FlatMirrStartPhi,
                                           360 - RichTbR1FlatMirrStartPhi, 0.0,
                                           180.0);
}break;
case 8: {//phi
solid =  new G4Sphere ("phi",0.0,
                                           RichTbR1FlatMirrOuterRadius,RichTbR1FlatMirrStartPhi,
                                           RichTbR1FlatMirrDeltaPhi, 0.0,
                                           180.0);
}break;
case 9: {//variety sphere
solid =  new G4Sphere ("variety sphere",RichTbR1FlatMirrInnerRadius,
                                           RichTbR1FlatMirrOuterRadius,RichTbR1FlatMirrStartPhi,
                                           RichTbR1FlatMirrDeltaPhi, RichTbR1FlatMirrStartTheta,
                                           RichTbR1FlatMirrDeltaTheta);
}break;
case 10: {//Orb
solid = new G4Orb("orb",RichTbR1FlatMirrOuterRadius);
}break;
case 11: {//box
const G4double& InnerRadius  = RichTbR1FlatMirrInnerRadius ; 
      const G4double& OuterRadius  = RichTbR1FlatMirrOuterRadius ; 
      const G4double& Thickness    = RichTbR1FlatMirrThickness ; 
      const G4double& SegmentSizeY = RichTbR1FlatMirrSegmentSizeY ; 
      const G4double& SegmentSizeZ = RichTbR1FlatMirrSegmentSizeX ;   // the SizeX is misnamed, should be SizeZ 

      const G4double SagittaMax = 1.*CLHEP::mm ;  // see ~/opticks/extg4/X4SolidMaker.cc X4SolidMaker::LHCbRichFlatMirr
      const G4double FullDepthX =  Thickness + SagittaMax ;  
      const G4double MiddleRadius = (InnerRadius + OuterRadius)/2. ;
solid = new G4Box("box", FullDepthX/2. , SegmentSizeY/2. , SegmentSizeZ/2. ); 
}break;
case 12: {//box intersect
const G4double& InnerRadius  = RichTbR1FlatMirrInnerRadius ; 
      const G4double& OuterRadius  = RichTbR1FlatMirrOuterRadius ; 
      const G4double& Thickness    = RichTbR1FlatMirrThickness ; 
      const G4double& SegmentSizeY = RichTbR1FlatMirrSegmentSizeY ; 
      const G4double& SegmentSizeZ = RichTbR1FlatMirrSegmentSizeX ;   // the SizeX is misnamed, should be SizeZ 

      const G4double SagittaMax = 1.*CLHEP::mm ;  // see ~/opticks/extg4/X4SolidMaker.cc X4SolidMaker::LHCbRichFlatMirr
      const G4double FullDepthX =  Thickness + SagittaMax ;  
      const G4double MiddleRadius = (InnerRadius + OuterRadius)/2. ;
G4Box* box = new G4Box("box", FullDepthX/2. , SegmentSizeY/2. , SegmentSizeZ/2. ); 
G4Box* box2 = new G4Box("box", FullDepthX/4. , SegmentSizeY/4. , SegmentSizeZ/4. ); 
solid  = new G4IntersectionSolid( "box-box intersection", box, box2, 0, G4ThreeVector( MiddleRadius, 0., 0. ));
}break;
case 13: {//box difference
const G4double& InnerRadius  = RichTbR1FlatMirrInnerRadius ; 
      const G4double& OuterRadius  = RichTbR1FlatMirrOuterRadius ; 
      const G4double& Thickness    = RichTbR1FlatMirrThickness ; 
      const G4double& SegmentSizeY = RichTbR1FlatMirrSegmentSizeY ; 
      const G4double& SegmentSizeZ = RichTbR1FlatMirrSegmentSizeX ;   // the SizeX is misnamed, should be SizeZ 

      const G4double SagittaMax = 1.*CLHEP::mm ;  // see ~/opticks/extg4/X4SolidMaker.cc X4SolidMaker::LHCbRichFlatMirr
      const G4double FullDepthX =  Thickness + SagittaMax ;  
      const G4double MiddleRadius = (InnerRadius + OuterRadius)/2. ;
G4Box* box = new G4Box("box", FullDepthX/2. , SegmentSizeY/2. , SegmentSizeZ/2. ); 
G4Box* box2 = new G4Box("box", FullDepthX/4. , SegmentSizeY/4. , SegmentSizeZ/4. ); 
solid  = new G4IntersectionSolid( "box-box difference", box, box2, 0, G4ThreeVector( MiddleRadius, 0., 0. ));
}break;
case 14: {//box difference
const G4double& InnerRadius  = RichTbR1FlatMirrInnerRadius ; 
      const G4double& OuterRadius  = RichTbR1FlatMirrOuterRadius ; 
      const G4double& Thickness    = RichTbR1FlatMirrThickness ; 
      const G4double& SegmentSizeY = RichTbR1FlatMirrSegmentSizeY ; 
      const G4double& SegmentSizeZ = RichTbR1FlatMirrSegmentSizeX ;   // the SizeX is misnamed, should be SizeZ 

      const G4double SagittaMax = 1.*CLHEP::mm ;  // see ~/opticks/extg4/X4SolidMaker.cc X4SolidMaker::LHCbRichFlatMirr
      const G4double FullDepthX =  Thickness + SagittaMax ;  
      const G4double MiddleRadius = (InnerRadius + OuterRadius)/2. ;
G4Box* box = new G4Box("box", FullDepthX/2. , SegmentSizeY/2. , SegmentSizeZ/2. ); 
G4Box* box2 = new G4Box("box", FullDepthX/4. , SegmentSizeY/4. , SegmentSizeZ/4. ); 
solid  = new G4SubtractionSolid( "box-box difference", box, box2, 0, G4ThreeVector( MiddleRadius, 0., 0. ));
}break;
case 15: {
solid = new G4Para ("parabola", 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);
}break;
case 16:{
solid = new G4Cons ("cone", 1.0, 2.0, 3.0, 4.0, 1.0, 45.0, 90.0);
}break;
case 17:{
solid = new G4Tubs ("Tube", 0.0, 10.0, 1.0, 0.0, 360.0);
}break;
case 18:{
solid = new G4Torus ("Torus", RichTbR1FlatMirrInnerRadius, RichTbR1FlatMirrOuterRadius, 2.0, 0.0, 360.0);
}break;
}



   G4RotationMatrix MirrFlatRotationX,MirrFlatRotationY;
   MirrFlatRotationX.rotateX(RichTbR1FlatMirrRotX);
   MirrFlatRotationY.rotateY(RichTbR1FlatMirrRotY);
   G4ThreeVector  MirrFlatPos (RichTbR1FlatMirrPosX,RichTbR1FlatMirrPosY,RichTbR1FlatMirrPosZ);

   
   G4Transform3D MirrFlatTransform (MirrFlatRotationX * MirrFlatRotationY, MirrFlatPos  ) ;
   rTbR1FlatMirrorLVol = 
        new G4LogicalVolume(solid, aMaterial->getMirrorQuartz(),RichTbR1FlatMirrorLogName,0,0,0);
   rTbR1FlatMirrorPVol = new G4PVPlacement(MirrFlatTransform,RichTbR1FlatMirrorPhysName,rTbR1FlatMirrorLVol,
					   mTbLHCbR1SubMaster->getRichTbLHCbRich1SubMasterPhysicalVolume(),false,1);

}
